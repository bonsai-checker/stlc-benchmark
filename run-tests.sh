#!/bin/bash

mkdir -p full/
mkdir -p log/
rm full/*
ln -s ../tree-lib.rkt full/tree-lib.rkt

for i in $(ls patch | sed s/\.patch//g); do
    echo Testing "$i"
    patch -s stlc.rkt -i patch/$i.patch -o full/$i.rkt;
    /usr/bin/time -p racket full/$i.rkt 2>&1 | grep real | tail -1 | sed "s/real *//" >> log/$i.log
done
