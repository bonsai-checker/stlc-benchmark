Prerequisites:
- Racket 6.8 or higher
  [ http://racket-lang.org ]
- Rosette 2.1 or higher
  [ http://emina.github.io/rosette/ ]
